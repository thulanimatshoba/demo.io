[![Build Status](https://travis-ci.org/Automattic/_s.svg?branch=master)](https://travis-ci.org/Automattic/_s)

_demo.oi
===

Hi. I'm a starter boilerplate project called `_demo`, or `demo.io`, if you like. I'm a starter project meant for quickly setting up your wordpress environment files...

Note: This will pull down the following repositories...

* "johnpbloch/`wordpress`": "5.*",
* "wpackagist-plugin/`kirki`": "3.0.*",
* "wpackagist-plugin/`woocommerce`": "3.*",
* "wpackagist-plugin/`wordpress-seo`": "11.*",
* "thulanimatshoba/`sapawu`": "dev-develop",
* "thulanimatshoba/`skinny-ninjah`": "dev-develop"



Getting Started
---------------

1. `composer update -vvv && composer install -vvv`

Now you're ready to go! The next step is easy to say, but harder to do: create some awesome code. :)

Good luck!